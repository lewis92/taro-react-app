import { ScrollView, View } from "@tarojs/components";
import NUBarChart from "@/components/NUBarChart";
import { BarChartProps } from "@/components/types/index";

export default function BarChart() {
  const list = [
    {
      title: "基本用法",
      options: {
        legend: {
          xPosition: "right",
          yPosition: "bottom",
        },
        xAxis: {
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        },
        yAxis: [
          {
            name: "流量",
            data: [112, 64, 47, 86, 121, 54, 143],
          },
        ],
      } as BarChartProps,
    },
    {
      title: "隐藏顶部数据",
      options: {
        legend: {
          isHide: true,
        },
        xAxis: {
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        },
        yAxis: [
          {
            name: "流量",
            width: 45,
            isHideValue: true,
            data: [112, 64, 47, 0, 121, 54, 143],
          },
        ],
      } as BarChartProps,
    },
    {
      title: "多条数据",
      options: {
        xAxis: {
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        },
        yAxis: [
          {
            name: "流量",
            data: [112, 64, 47, 30, 121, 54, 143],
          },
          {
            name: "访问人数",
            data: [43, 52, 66, 123, 90, 64, 80],
          },
        ],
      } as BarChartProps,
    },
    {
      title: "调整柱状图宽高",
      options: {
        grid: {
          height: 300,
        },
        xAxis: {
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        },
        yAxis: [
          {
            name: "流量",
            data: [112, 64, 47, 86, 121, 54, 143],
          },
        ],
      } as BarChartProps,
    },
    {
      title: "修改X轴样式",
      options: {
        grid: {
          height: 400,
        },
        xAxis: {
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
          rotate: 50,
          bottom: "-34%",
          labelStyle: {
            color: "#3a5da0",
            fontWeight: "600",
          } as CSSStyleDeclaration,
        },
        yAxis: [
          {
            name: "流量",
            data: [112, 64, 47, 50, 121, 54, 143],
          },
        ],
      } as BarChartProps,
    },
    {
      title: "修改颜色",
      options: {
        xAxis: {
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        },
        yAxis: [
          {
            name: "流量",
            data: [112, 64, 47, 60, 121, 54, 143],
            backgroundColor: "#ff9845",
          },
          {
            name: "订单量",
            data: [43, 52, 66, 123, 90, 64, 80],
            backgroundColor: "linear-gradient(to bottom, #a955f6, #ea489c)",
          },
        ],
      } as BarChartProps,
    },
    {
      title: "柱状成半圆形",
      options: {
        xAxis: {
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        },
        yAxis: [
          {
            name: "订单量",
            type: "semiCircle",
            backgroundColor: "#6ec8eb",
            data: [43, 52, 66, 123, 90, 64, 80],
          },
        ],
      } as BarChartProps,
    },
    {
      title: "柱状成方形",
      options: {
        xAxis: {
          data: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        },
        yAxis: [
          {
            name: "订单量",
            type: "square",
            backgroundColor: "#945fb8",
            data: [43, 52, 66, 123, 90, 64, 80],
          },
        ],
      } as BarChartProps,
    },
  ];

  return (
    <ScrollView className="p-4 box-border bg-[#f7f7f7]">
      <View className="font-bold mx-4 mb-6">柱状图</View>
      {list.map((item, index) => (
        <View
          key={index}
          className="rounded-[30px] bg-white p-4 drop-shadow-xl mb-8"
        >
          <View className="font-bold text-lg mb-4">{item.title}</View>
          <NUBarChart options={item.options} />
        </View>
      ))}
    </ScrollView>
  );
}
