import { View, Text, Image } from "@tarojs/components";
import { collapseItemProps } from "@/components/types";
import NUCollapse from "@/components/NUCollapse";
import Taro from "@tarojs/taro";
import logoPNG from "@/assets/images/logo.png";
import "./index.scss";

export default function Index() {
  const list: collapseItemProps[] = [
    {
      id: "chart",
      title: "图表",
      icon: "barchart",
      childrens: [
        { id: "barChart", title: "柱状图", path: "/pages/barChart/index" },
        { id: "1-2", title: "菜单1-2", path: "" },
      ],
    },
    {
      id: "2",
      title: "菜单2",
      icon: "more",
      childrens: [
        { id: "2-1", title: "菜单2-1" },
        { id: "2-2", title: "菜单2-2" },
        { id: "2-3", title: "菜单2-3" },
      ],
    },
  ];

  function onChangeCollapseItem({ curInfo }) {
    if (curInfo.path) {
      Taro.navigateTo({
        url: curInfo.path,
      });
    }
  }

  return (
    <View className="w-screen h-screen bg-[#f7f7f7] pt-10 px-5 box-border">
      <View className="flex items-end mb-5">
        <Image src={logoPNG} className="w-12 h-12 mr-2"></Image>
        <Text className="text-[50px] font-bold text-[#3f3f3f]">Street UI</Text>
      </View>
      <View className="mb-5 text-sm text-[#999]">
        Street UI 展示适配小程序界面设计、交互等示例。
        <View>持续更新中，敬请期待！</View>
      </View>
      <NUCollapse
        list={list}
        onChangeCollapseItem={({ curInfo }) =>
          onChangeCollapseItem({ curInfo })
        }
      />
      <View className="text-sm text-center text-[#999] mt-20">
        该小程序仅演示示例，不收集个人信息。
      </View>
    </View>
  );
}
