import { useState } from "react";
import { View, Text } from "@tarojs/components";
import { collapseItemProps } from "@/components/types";
import NUIconFont from "../NUIconFont";
import "./index.scss";

interface Props {
  list: collapseItemProps[];
  onChangeCollapseItem?: ({ parent, curInfo }) => void;
}
// 折叠面板
export default function NUCollapse({ list, onChangeCollapseItem }: Props) {
  const itemHeight = 3.8;
  const itemClass =
    "text-[32px] bg-white px-[40px] box-border flex items-center justify-between";
  const [curIndex, setCurIndex] = useState("");

  function onChange(id) {
    if (id === curIndex) {
      setCurIndex("");
    } else {
      setCurIndex(id);
    }
  }

  function onChangeItem({ parent, curInfo }) {
    onChangeCollapseItem && onChangeCollapseItem({ parent, curInfo });
  }

  return (
    <View>
      {list.map((item) => (
        <View key={item.id} className="mb-[24px]">
          <View
            key={item.id}
            className={`rounded-[8px] py-5 ${itemClass} ${
              curIndex === item.id ? "text-[#999] rounded-b-[0px]" : ""
            }`}
            onClick={() => onChange(item.id)}
          >
            <Text>{item.title}</Text>
            <NUIconFont name={item.icon} color="#999" />
          </View>
          {item.childrens?.length && (
            <View
              className="transition-all overflow-hidden"
              style={{
                height: `${
                  curIndex === item.id
                    ? `${itemHeight * item.childrens.length}rem`
                    : "0"
                }`,
              }}
            >
              {item.childrens.map((child) => (
                <View
                  key={child.id}
                  style={{ height: `${itemHeight}rem` }}
                  className={`box-border relative overflow-hidden children-item ${itemClass}`}
                  onClick={() => {
                    onChangeItem({
                      parent: item,
                      curInfo: child,
                    });
                  }}
                >
                  <Text>{child.title}</Text>
                  <NUIconFont name="right" size={16} color="#999" />
                </View>
              ))}
            </View>
          )}
        </View>
      ))}
    </View>
  );
}
