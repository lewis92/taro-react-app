import Taro from "@tarojs/taro";
import { View } from "@tarojs/components";
import { BarChartProps } from "@/components/types/index";
import useSetList from "./hooks/useSetList";
import "./index.scss";

export default function NUBarChart(props: { options: BarChartProps }) {
  const { list, legendList, legendStyle } = useSetList(props.options);
  const { legend, grid } = props.options;

  return (
    <View
      style={{
        width: `${grid?.width ? Taro.pxTransform(grid.width) : ""}`,
        height: `${Taro.pxTransform(grid?.height ? grid.height : 500)}`,
      }}
      className="relative box-border py-10 overflow-hidden"
    >
      {/* 柱状图主体 */}
      <View className="flex items-end h-[90%] pl-4 pb-8 overflow-x-scroll scroll-box">
        {list.map((item, index) => (
          <View
            key={index}
            className="flex items-end h-full relative bar-box mr-6"
          >
            {item.childrens.map((child, idx) => (
              <View
                key={idx}
                style={{
                  width: `${Taro.pxTransform(child.width)}`,
                  height: `${child.height}%`,
                  background: `${child.backgroundColor}`,
                }}
                className={`mr-1 relative bar-box ${
                  child.type === "semiCircle"
                    ? "rounded-t-[80px]"
                    : child.type === "square"
                    ? "rounded-0"
                    : "rounded-[80px]"
                }`}
              >
                {/* 顶部数值 */}
                <View
                  style={{ transform: "translateX(-50%)", ...child.valueStyle }}
                  className={`text-[24px] text-slate-500 absolute -top-6 left-[50%] value-box ${
                    !child.value || !child.height || child.isHideValue
                      ? "opacity-0"
                      : ""
                  }`}
                >
                  {child.value.toLocaleString()}
                </View>
              </View>
            ))}
            {/* X轴 */}
            <View
              style={{
                transform: `rotate(${item.rotate || 0}deg) translateX(-50%)`,
                bottom: `${item.bottom || Taro.pxTransform(-40)}`,
                ...item.labelStyle,
              }}
              className="text-[24px] text-slate-500 absolute left-[50%]"
            >
              {item.label}
            </View>
          </View>
        ))}
      </View>
      {/* 图示 */}
      {!legend || !legend.isHide ? (
        <View className="absolute flex items-center" style={{ ...legendStyle }}>
          {legendList.map((item, index) => (
            <View key={index} className="flex items-center text-[22px] mr-4">
              <View
                style={{ background: item.backgroundColor }}
                className="w-3 h-2 rounded-[4px] mr-1"
              ></View>
              <View>{item.name}</View>
            </View>
          ))}
        </View>
      ) : null}
    </View>
  );
}
