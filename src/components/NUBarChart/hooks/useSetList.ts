import { useEffect, useState } from "react";
import { BarChartProps } from "@/components/types/index";

export default function useSetList(props: BarChartProps) {
  const [list, setList] = useState([]) as any;
  const [legendList, setLegendList] = useState([]) as any;
  const [legendStyle, setLegendStyle] = useState({
    top: "0",
    left: "50%",
    transform: "translateX(-50%)",
  }) as any;

  const colors = [
    "#5b91fa",
    "#59d8a6",
    "#5c6f92",
    "#f7bd16",
    "#e86453",
    "#6ec8eb",
    "#945fb8",
    "#ff9845",
    "#1d9394",
  ];

  const { xAxis, yAxis, legend } = props;
  const data = yAxis.map((item) => item.data);
  const flatData = [...new Set(data.flat())]; // 多维数组转成一维数组
  const maxData = Math.max(...flatData.map((item) => item || 0)); // 最大值
  const paddingHeight = 16; // 柱状到顶部距离

  useEffect(() => {
    const newList = xAxis.data.map((item, index) => {
      const childrens = data.map((d, idx) => ({
        ...yAxis[idx],
        value: d[index],
        height: 0,
        width: yAxis[idx].width || 30,
        backgroundColor: yAxis[idx]?.backgroundColor || colors[idx],
      }));

      return {
        ...xAxis,
        label: item,
        childrens,
      };
    });

    setList(newList);

    // 设置图示
    const newLegendList = yAxis.map((item, index) => ({
      name: item.name,
      backgroundColor: item.backgroundColor || colors[index],
    }));
    setLegendList(newLegendList);

    // 设置图示位置
    if (legend && !legend.isHide) {
      const style: any = { transform: [] };
      if (legend.xPosition) {
        switch (legend.xPosition) {
          case "left":
            style.left = 0;
            break;
          case "right":
            style.right = 0;
            break;
          default:
            style.left = "50%";
            style.transform.push("translateX(-50%)");
            break;
        }
      }
      if (legend.yPosition) {
        switch (legend.yPosition) {
          case "top":
            style.top = 0;
            break;
          case "bottom":
            style.bottom = 0;
            break;
          default:
            style.top = "50%";
            style.transform.push("translateY(-50%)");
            break;
        }
      }
      const styleStr = {};
      for (const item in style) {
        if (item === "transform" && style[item].length) {
          styleStr[item] = style[item].join(" ");
        } else {
          styleStr[item] = style[item];
        }
      }
      setLegendStyle(styleStr);
    }

    // 设置柱状高度 实现动画效果
    const setHeight = (value) => {
      const height = Math.trunc((value / maxData) * 100) - paddingHeight;
      return height < 0.5 ? 0.5 : height;
    };
    setTimeout(() => {
      const newHeightList = newList.map((item) => ({
        ...item,
        childrens: item.childrens.map((child) => ({
          ...child,
          height: setHeight(child.value),
        })),
      }));
      setList(newHeightList);
    }, 500);
  }, [props]);

  return {
    list,
    legendList,
    legendStyle,
  };
}
