interface collapseItemProps {
  title: string;
  id: string;
  path?: string;
  icon?: any;
  childrens?: collapseItemProps[];
}

export { collapseItemProps };
