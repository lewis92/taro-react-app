import { IconFont } from "@nutui/icons-react-taro";
import { View } from "@tarojs/components";

interface Props {
  name: string; // 图标名称或图片链接
  color?: string; // 图标颜色
  size?: string | number; // 图标大小，如 20px 2em 2rem
  classPrefix?: string; // 类名前缀，用于使用自定义图标
  fontClassName?: string; // 自定义 icon 字体基础类名
  tag?: string; // tsx 标签
  className?: any;
  style?: any;
}

// 图标
export default function NUIconFont(props: Props) {
  const { size = 24, fontClassName = "iconfont", classPrefix = "icon" } = props;
  return (
    <View>
      <IconFont
        size={size}
        color={props.color}
        fontClassName={fontClassName}
        classPrefix={classPrefix}
        name={props.name}
        className={props.className}
        style={props.style}
      />
    </View>
  );
}
