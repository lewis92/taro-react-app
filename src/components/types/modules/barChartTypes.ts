// 柱状图 type

interface GridProps {
  width?: number;
  height?: number;
}

interface XAxisProps {
  data: string[];
  rotate?: number; // 旋转角度
  bottom?: string;
  labelStyle?: CSSStyleDeclaration; // X轴样式
}

interface YAxisProps {
  name: string;
  data: number[];
  type?: "round" | "semiCircle" | "square"; // 圆形 | 半圆形 | 方形
  isHideValue?: boolean; // 是否隐藏顶部数据
  backgroundColor?: string; // 柱状颜色
  width?: number; // 柱状宽度
  valueStyle?: CSSStyleDeclaration; // 数值样式
}

type XPositionProps = "left" | "center" | "right";
type YPositionProps = "top" | "center" | "bottom";

interface LegendProps {
  isHide?: boolean;
  xPosition?: XPositionProps;
  yPosition?: YPositionProps;
}

interface BarChartProps {
  xAxis: XAxisProps;
  yAxis: YAxisProps[];
  grid?: GridProps;
  legend?: LegendProps;
}

export { BarChartProps };
